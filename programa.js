var contexto;

function inicializar() {
    let lienzo = document.getElementById('miLienzo');
    lienzo.width = window.innerWidth;
    lienzo.height = window.innerHeight;
    contexto = lienzo.getContext('2d'); //retorna un contexto de dibujo en el lienzo en 2D
    contexto.beginPath();
    contexto.lineWidth = 1;
    contexto.strokeStyle = "#CCCCCC"; // propone el color, gradiante alrededor de una figura
    contexto.moveTo(0, (window.innerHeight / 2) - 100); //mueve la ventana actual a coordenadas específicas
    contexto.lineTo(window.innerWidth, (window.innerHeight / 2) - 100); //línea recta (x,y)
    contexto.stroke(); //Ayuda a dibujar la línea en el lienzo
    contexto.translate(0, window.innerHeight / 2); //recoloca un elemento en el eje horizontal y/o vertical, coordenadas definen cuanto se movera el objeto
    contexto.scale(1, -1); //modifica el tamaño de un elemento, se encoge si esta dentro de [-1, 1s]
}

function dibujar() {
    dibujo.fillStyle = 'blue'; //aplica un relleno de color a un rectángulo
    dibujo.fillRect(0, 0, 50, 100);
}


function dibujarCirculo(x, y, radio) {
    contexto.beginPath();
    contexto.arc(x, y, radio, 0, 2 * Math.PI, true); //circulo, (x, y, radio, inicio de ángulo, fin de ángulo, sentido)
    contexto.fill(); //llena el dibujo actuañ
}

function botonSeno(frecuencia) {
    inicializar();
    let x;
    let yoffset = 100;
    let amplitud = 100;
    for (x = 0; x < 360 * 4; x += (1 / frecuencia)) {
        y = Math.sin(x * frecuencia * Math.PI / 180) * amplitud + yoffset;
        dibujarCirculo(x, y, 1);
    }
}

function botonCoseno(frecuencia) {
    inicializar();
    let x;
    let yoffset = 100;
    let amplitud = 100;
    for (x = 0; x < 360 * 4; x += (1 / frecuencia)) {
        y = Math.cos(x * frecuencia * Math.PI / 180) * amplitud + yoffset;
        dibujarCirculo(x, y, 1);
    }
}

function dibujarTrigonometrica(opcion, frecuencia) {
    let x;
    let yoffset = 100;
    let amplitud = 100;
    for (x = 0; x < 360 * 4; x += (1 / frecuencia)) {
        if (opcion === 'seno') {
            console.log(document.getElementById("botonSeno"));
            y = Math.sin(x * frecuencia * Math.PI / 180) * amplitud + yoffset;
        } else if (opcion === 'coseno') {
            console.log(document.getElementById("botonCoseno"));
            y = Math.cos(x * frecuencia * Math.PI / 180) * amplitud + yoffset;

        }
        dibujarCirculo(x, y, 1);
    }

}
async function dibujarSennales() {

    for (i = 1; i < 100; i++) {
        await sleep(200);
        dibujarTrigonometrica('coseno', i);
    }

}